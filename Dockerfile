FROM        docker.io/library/nginx:latest

# Envsubst templates and copy them to /usr/share/nginx/html/ and /etc/nginx/
RUN         rm -rf /usr/share/nginx/html/*
COPY        /dist /usr/share/nginx/template/html/
COPY        nginx/* /etc/template/nginx/

COPY        docker-entrypoint.d/* /docker-entrypoint.d/
RUN         chmod +x /docker-entrypoint.d/*

EXPOSE      80

STOPSIGNAL  SIGTERM

CMD         ["nginx", "-g", "daemon off;"]