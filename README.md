# АИС Поликлиники

## Установка пакетов
```
npm install
```

### Запуск приложения в режиме разработки
```
npm run serve
```

## Jira

```
https://cloudtech42.atlassian.net/jira/software/projects/CT/boards/1
```

## Схема базы данных

![Структура БД](project_img/dbcloud.png)
