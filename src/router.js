import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home'
import SignIn from './views/SignInPage'
import ToDos from './views/Todos'
import Page1 from './views/Page1'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: `${process.env.BASE_URL}`,
      component: Home
    },
    {
      path: `${process.env.BASE_URL}todos`,
      component: ToDos
    },
    {
      path: `${process.env.BASE_URL}page1`,
      component: Page1
    },
    {
      path: `${process.env.BASE_URL}signin`,
      component: SignIn
    }
  ]
})