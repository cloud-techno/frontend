#!/bin/sh

set -e

manual_envsubst() {
  mkdir -p /usr/share/nginx/html/css
  mkdir -p /usr/share/nginx/html/js

  for f in /usr/share/nginx/template/html/**/*
  do
    echo "envsubst template $f"
    envsubst '${PUBCLIC_SUBPATH}' < "$f" > "/usr/share/nginx/${f##/usr/share/nginx/template/}"
  done

  cp /usr/share/nginx/template/html/favicon.ico /usr/share/nginx/html/

  echo "envsubst template /usr/share/nginx/template/html/index.html"
  envsubst '${PUBCLIC_SUBPATH}' < "/usr/share/nginx/template/html/index.html" > "/usr/share/nginx/html/index.html"

  echo "envsubst template /etc/template/nginx/nginx.conf"
  envsubst '${PUBCLIC_SUBPATH}' < "/etc/template/nginx/nginx.conf" > "/etc/nginx/nginx.conf"
}

manual_envsubst

exit 0
